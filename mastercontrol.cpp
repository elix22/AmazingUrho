/* A Mazing Urho
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "effectmaster.h"
#include "inputmaster.h"
#include "player.h"
#include "spawnmaster.h"
#include "amazingcam.h"

#include "block.h"
#include "weed.h"
#include "maze.h"
#include "urho.h"
#include "shrimp.h"
#include "barrelfactory.h"
#include "barrel.h"
#include "jelly.h"
#include "crab.h"

#include "waterspotlight.h"
#include "menu.h"

#include "mastercontrol.h"

URHO3D_DEFINE_APPLICATION_MAIN(MasterControl);

MasterControl* MasterControl::instance_ = NULL;

MasterControl* MasterControl::GetInstance()
{
    return MasterControl::instance_;
}

MasterControl::MasterControl(Context *context):
    Application(context),
    spotNodes_{}
{
    instance_ = this;
}

void MasterControl::Setup()
{
    SetRandomSeed(TIME->GetSystemTime());

    engineParameters_[EP_LOG_NAME] = GetSubsystem<FileSystem>()->GetAppPreferencesDir("luckey", "logs") + "AmazingUrho.log";
    engineParameters_[EP_WINDOW_TITLE] = "A-Mazing Urho";
    engineParameters_[EP_WINDOW_ICON] = "icon.png";
    engineParameters_[EP_WORKER_THREADS] = false;
    engineParameters_[EP_RESOURCE_PATHS] = "Resources;";


//    engineParameters_[EP_FULL_SCREEN] = false;
//    engineParameters_[EP_WINDOW_RESIZABLE] = true;
}
void MasterControl::Start()
{


    RegisterSubsystem<EffectMaster>();
    RegisterSubsystem<InputMaster>();
    RegisterSubsystem<SpawnMaster>();

    RegisterObject<AmazingCam>();
    RegisterObject<Block>();
    RegisterObject<Weed>();
    RegisterObject<Maze>();
    RegisterObject<Urho>();
    RegisterObject<Shrimp>();
    RegisterObject<BarrelFactory>();
    RegisterObject<Barrel>();
    RegisterObject<Jelly>();
    RegisterObject<Crab>();

    RegisterObject<WaterSpotLight>();
    RegisterObject<Menu>();

    if (GRAPHICS)
        ENGINE->SetMaxFps(GRAPHICS->GetRefreshRate());


    ingameIntro_ = CACHE->GetResource<Sound>("Music/IngameIntro.ogg");
    ingameIntro_ ->SetLooped(false);
    ingameLoop_ = CACHE->GetResource<Sound>("Music/IngameLoop.ogg");
    ingameLoop_ ->SetLooped(true);

    CreateScene();

    SubscribeToEvent(E_SCREENMODE, URHO3D_HANDLER(MasterControl, HandleScreenMode));
}
void MasterControl::Stop()
{
    engine_->DumpResources(true);
}
void MasterControl::Exit()
{
    engine_->Exit();
}

void MasterControl::CreateScene()
{
    scene_ = new Scene(context_);
    scene_->CreateComponent<Octree>();
//    scene_->CreateComponent<PhysicsWorld>();
    scene_->CreateComponent<DebugRenderer>();

    Zone* zone{ scene_->CreateComponent<Zone>() };
    zone->SetBoundingBox(BoundingBox(Vector3(-100.0f, -50.0f, -100.0f), Vector3(100.0f, 50.0f, 100.0f)));
    zone->SetAmbientColor(Color(0.1f, 0.17f, 0.23f));
    zone->SetFogStart(23.0f);
    zone->SetFogEnd(235.0f);
    zone->SetFogColor(zone->GetAmbientColor());

    //Lights
    Node* pointLightNode{ scene_->CreateChild("Light") };
    pointLightNode->SetPosition(Vector3(3.0f, 13.0f, 2.0f));
    pointLightNode->LookAt(Vector3::DOWN);
    Light* pointLight{ pointLightNode->CreateComponent<Light>() };
    pointLight->SetRange(55.0f);
    pointLight->SetBrightness(0.42f);

    Node* sunLightNode{ scene_->CreateChild("Light") };
    sunLightNode->SetPosition(Vector3(-3.0f, 10.0f, -1.0f));
    sunLightNode->LookAt(Vector3::ZERO);
    Light* sunLight{ sunLightNode->CreateComponent<Light>() };
    sunLight->SetLightType(LIGHT_DIRECTIONAL);
    sunLight->SetCastShadows(true);
    sunLight->SetShadowIntensity(0.666f);
    sunLight->SetShadowDistance(17.0f);
    sunLight->SetRange(23.0f);
    sunLight->SetBrightness(0.42f);
    sunLight->SetSpecularIntensity(0.0f);
    sunLight->SetColor(Color(0.6f, 0.9f, 1.0f));

    for (bool turned: {false, true}) {

        Node* spotLightNode{ scene_->CreateChild("Light") };
        spotLightNode->CreateComponent<WaterSpotLight>()->Set(turned);

        spotNodes_.Push(spotLightNode);
    }

    //Camera
    camera_ = scene_->CreateChild("Camera")->CreateComponent
            <AmazingCam>();

    //The menu
    menu_ = scene_->CreateChild("Menu")->CreateComponent
            <Menu>();

    camera_->Set(menu_->GetCameraProperties());

    //A Maze!
    maze_ = scene_->CreateChild("Maze")->CreateComponent
            <Maze>();

    //Music!
    Sound* music{CACHE->GetResource<Sound>("Music/Menu.ogg")};
    music->SetLooped(true);
//    music->SetLoop();
    SoundSource* musicSource{scene_->CreateComponent<SoundSource>()};
//    musicSource->SetGain(0.0f);
    musicSource->SetSoundType(SOUND_MUSIC);
    musicSource->Play(music);
}
void MasterControl::HandleScreenMode(StringHash eventType, VariantMap& eventData)
{
    if (!menu_->IsEnabled())
        camera_->Set(maze_->GetCameraProperties());
}
void MasterControl::StartNewGame()
{
    menu_->SetEnabled(false);
    maze_->LoadBlockMap("Maps/Level9.emp");

    camera_->Set(maze_->GetCameraProperties());

    Zone* zone{ scene_->GetComponent<Zone>() };
    zone->SetFogStart(M_INFINITY);
    zone->SetFogEnd(M_INFINITY);

    scene_->GetComponent<SoundSource>()->Play(ingameIntro_);
    SubscribeToEvent(scene_, E_SOUNDFINISHED, URHO3D_HANDLER(MasterControl, HandleSoundFinished));
}
void MasterControl::HandleSoundFinished(StringHash eventType, VariantMap& eventData)
{
    SoundSource* musicSource{ scene_->GetComponent<SoundSource>() };

    if (musicSource->GetSound()->GetName().Contains("Ingame"))
    {
        musicSource->Play(ingameLoop_);
        UnsubscribeFromEvent(scene_, E_SOUNDFINISHED);
    }
}
void MasterControl::Winner()
{
    SoundSource* musicSource{ scene_->GetComponent<SoundSource>() };

    Sound* music{ CACHE->GetResource<Sound>("Music/VictoriousFanfare.ogg") };
    music->SetLooped(false);
    musicSource->Play(music);
}
Vector<SharedPtr<Player> > MasterControl::GetPlayers()
{
    return players_;
}
Player* MasterControl::GetPlayer(int playerId) const
{
    for (Player* p : players_) {

        if (p->GetPlayerId() == playerId){
            return p;
        }
    }
    return nullptr;
}
Player* MasterControl::GetNearestPlayer(Vector3 pos)
{
    Player* nearest{};
    for (Player* p : players_){
        if (p->IsAlive()){

            if (!nearest
                    || (LucKey::Distance(GetSubsystem<InputMaster>()->GetControllableByPlayer(p->GetPlayerId())->GetWorldPosition(), pos) <
                        LucKey::Distance(GetSubsystem<InputMaster>()->GetControllableByPlayer(nearest->GetPlayerId())->GetWorldPosition(), pos)))
            {
                nearest = p;
            }
        }
    }
    return nearest;
}

float MasterControl::Sine(const float freq, const float min, const float max, const float shift)
{
    float phase{ SinePhase(freq, shift) };
    float add{ 0.5f * (min + max) };

    return LucKey::Sine(phase) * 0.5f * (max - min) + add;
}
float MasterControl::Cosine(const float freq, const float min, const float max, const float shift)
{
    return Sine(freq, min, max, shift + 0.25f);
}
float MasterControl::SinePhase(float freq, float shift)
{
    return M_PI * 2.0f * (freq * scene_->GetElapsedTime() + shift);
}
