/* A Mazing Urho
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "weed.h"

StaticModelGroup* Weed::weedGroup_{};

Weed::Weed(Context* context) : SceneObject(context),
    clockwise_{Random(2)}
{
}

void Weed::OnNodeSet(Node* node)
{
    if (!node)
        return;


    if (!weedGroup_) {

        weedGroup_ = node_->GetScene()->CreateComponent<StaticModelGroup>();
        weedGroup_->SetModel(CACHE->GetResource<Model>("Models/Weed.mdl"));
        weedGroup_->SetMaterial(CACHE->GetResource<Material>("Materials/VColNoCull.xml"));
        weedGroup_->SetCastShadows(false);
    }

    node_->Rotate(Quaternion(Random(360.0f), Vector3::UP));
    float fatness{ Random(0.7, 1.5f) };
    node_->SetScale(Vector3(fatness, Random(0.75, 1.5f), fatness));
    weedGroup_->AddInstanceNode(node_);
}

void Weed::Update(float timeStep)
{
    node_->Rotate(Quaternion((clockwise_ ? timeStep : -timeStep) * (42.0f + 23.0f * randomizer_), Vector3::UP));
}



