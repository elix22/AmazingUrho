/* A Mazing Urho
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef WEED_H
#define WEED_H

#include "sceneobject.h"


class Weed : public SceneObject
{
    URHO3D_OBJECT(Weed, SceneObject);
public:
    Weed(Context* context);
    virtual void OnNodeSet(Node* node);
    virtual void Update(float timeStep);
private:
    static StaticModelGroup* weedGroup_;
    bool clockwise_;
};

#endif // WEED_H
