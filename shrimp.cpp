/* A Mazing Urho
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "urho.h"
#include "spawnmaster.h"

#include "shrimp.h"

StaticModelGroup* Shrimp::shrimpGroup_{};

Shrimp::Shrimp(Context* context) : SceneObject(context),
    clockwise_{},
    shrink_{false}
{
}

void Shrimp::OnNodeSet(Node* node)
{
    if (!node)
        return;

    if (!shrimpGroup_) {

        shrimpGroup_ = node_->GetScene()->CreateComponent<StaticModelGroup>();
        shrimpGroup_->SetModel(CACHE->GetResource<Model>("Models/Shrimp.mdl"));
        shrimpGroup_->SetMaterial(CACHE->GetResource<Material>("Materials/VColHighSpec.xml"));
        shrimpGroup_->SetCastShadows(true);
    }

    modelNode_ = node_->CreateChild("Model");
    modelNode_->SetPosition((Random(2) ? Vector3::RIGHT : Vector3::LEFT) * Random(0.1f, 0.23f) + Vector3::UP * 0.17f);
    modelNode_->Rotate(Quaternion(Random(-5.0f, 23.0f), Vector3::RIGHT));
    clockwise_ = modelNode_->GetDirection().CrossProduct(modelNode_->GetPosition()).y_ > 0.0f;
    modelNode_->SetScale(Random(0.9, 1.1f));
    shrimpGroup_->AddInstanceNode(modelNode_);

    modelNode_->RotateAround(Vector3::ZERO, Quaternion(Random(-5.0f, 5.0f), Random(360.0f), Random(-5.0f, 5.0f)), TS_PARENT);
}

void Shrimp::Set(Vector3 position, Quaternion rotation)
{
    SceneObject::Set(position, rotation);
    node_->SetScale(1.0f);
    shrink_ = false;
}

void Shrimp::Update(float timeStep)
{
    modelNode_->RotateAround(Vector3::ZERO, Quaternion((clockwise_ ? timeStep : -timeStep) * (23.0f + 5.0f * randomizer_), Vector3::UP), TS_PARENT);

    if (!shrink_ && Urho::urho_->GetWorldPosition().DistanceToPoint(node_->GetWorldPosition()) < 0.666f) {

        PlaySample(CACHE->GetResource<Sound>("Samples/Crack.ogg"), 0.8f);
        shrink_ = true;
    }

    else if (shrink_) {
        float scale{ node_->GetScale().x_ - timeStep * 2.3f };
        if (scale < 0.0f) {

            if (LastShrimp()) {

                MC->Winner();
            }

            Disable();

        } else {
            node_->SetScale(scale);
        }
    }
}

bool Shrimp::LastShrimp()
{
    return GetSubsystem<SpawnMaster>()->CountActive<Shrimp>() == 1;
}

