/* A Mazing Urho
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "maze.h"

#include "jelly.h"

Jelly::Jelly(Context* context) : Hazard(context),
    modelNode_{},
    target_{}
{
}

void Jelly::OnNodeSet(Node* node)
{
    if (!node)
        return;

    modelNode_ = node_->CreateChild("Model");
    AnimatedModel* jellyModel{ modelNode_->CreateComponent<AnimatedModel>() };
    jellyModel->SetModel(CACHE->GetResource<Model>("Models/Jelly.mdl"));
    jellyModel->SetCastShadows(true);
    jellyModel->SetMaterial(CACHE->GetResource<Material>("Materials/VColHighSpec.xml"));
    animCtrl_ = node_->CreateComponent<AnimationController>();
    animCtrl_->PlayExclusive("Models/JellySwim.ani", 0, true);
    animCtrl_->SetSpeed("Models/JellySwim.ani", 0.0f);
}
void Jelly::Set(Vector3 position, Quaternion rotation)
{
    SceneObject::Set(position, rotation);
    target_ = node_->GetWorldPosition();
}
void Jelly::Update(float timeStep)
{
    Hazard::Update(timeStep);

    node_->SetWorldPosition(node_->GetPosition().Lerp(target_, timeStep * (target_.DistanceToPoint(node_->GetWorldPosition()) * 3.0f + 2.0f)));
    modelNode_->SetWorldRotation(modelNode_->GetWorldRotation().Slerp(node_->GetWorldRotation(), timeStep * 5.0f));

    animCtrl_->SetTime("Models/JellySwim.ani", (1.0f - target_.DistanceToPoint(node_->GetWorldPosition()) - 0.17f) * animCtrl_->GetLength("Models/JellySwim.ani"));

    if (target_.DistanceToPoint(node_->GetWorldPosition()) < 0.17f) {

        Maze* maze{ MC->GetMaze() };
        if (maze->IsObstacle(target_ + node_->GetDirection())) {
            Quaternion turn{ Quaternion(maze->IsObstacle(target_ + node_->GetRight()) ? -90.0f : 90.0f, Vector3::UP) };
            modelNode_->Rotate(turn.Inverse());
            node_->Rotate(turn);
        }

        target_ += node_->GetDirection();
    }
}



