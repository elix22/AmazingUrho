/* A Mazing Urho
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef MAZE_H
#define MAZE_H

#include "mastercontrol.h"


class Maze : public LogicComponent
{
    URHO3D_OBJECT(Maze, LogicComponent);
public:
    Maze(Context* context);
    virtual void OnNodeSet(Node* node);
    virtual void Update(float timeStep);
    void LoadBlockMap(String fileName);

    const HashSet<Vector3>& GetPits() const { return pits_; }
    IntVector3 WorldPositionToCoords(Vector3 position);
    bool IsObstacle(Vector3 position);
    bool IsObstacle(IntVector3 coords);
    const CameraProperties GetCameraProperties();
private:
    IntVector3 mapSize_;
    Vector3 blockSize_;
    HashSet<IntVector2> obstacles_;
    HashSet<Vector3> pits_;
    Vector3 MapSizeModulo();
};

#endif // MAZE_H
