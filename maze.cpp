/* A Mazing Urho
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "block.h"
#include "weed.h"
#include "spawnmaster.h"
#include "urho.h"
#include "shrimp.h"
#include "barrelfactory.h"
#include "jelly.h"
#include "crab.h"

#include "maze.h"

Maze::Maze(Context* context) : LogicComponent(context),
    mapSize_{},
    blockSize_{},
    obstacles_{},
    pits_{}
{
}

void Maze::OnNodeSet(Node* node)
{
    if (!node)
        return;

}

void Maze::Update(float timeStep)
{
}

void Maze::LoadBlockMap(String fileName) {


    node_->RemoveAllChildren();
    obstacles_.Clear();
    pits_.Clear();

    SpawnMaster* spawn{ GetSubsystem<SpawnMaster>() };
    ResourceCache* cache{ GetSubsystem<ResourceCache>() };

    XMLFile* mapXML{ cache->GetResource<XMLFile>(fileName) };

    XMLElement mapElem{ mapXML->GetRoot("blockmap") };
    mapSize_ = mapElem.GetIntVector3("map_size");
    blockSize_ = mapElem.GetVector3("block_size");

    HashMap<int, HashMap<int, Pair<String, String>>> blockSets;
    XMLElement blockSetRefElem{ mapElem.GetChild("blockset") };

    while(blockSetRefElem) {
        int blockSetId{ blockSetRefElem.GetInt("id") };

        XMLFile* blockSetXML{ cache->GetResource<XMLFile>(blockSetRefElem.GetAttribute("name")) };
        XMLElement blockSetElem{ blockSetXML->GetRoot("blockset") };

        XMLElement blockElem{ blockSetElem.GetChild("block") };
        while (blockElem) {

            blockSets[blockSetId][blockElem.GetInt("id")].first_ = blockElem.GetAttribute("model");
            blockSets[blockSetId][blockElem.GetInt("id")].second_ = blockElem.GetAttribute("material");

            blockElem = blockElem.GetNext("block");
        }

        blockSetRefElem = blockSetRefElem.GetNext("blockset");
    }

    //Create and fill obstacles coords set
    for (int x{0}; x < mapSize_.x_; ++x) {
        for (int y{0}; y < mapSize_.z_; ++y) {

            obstacles_.Insert(IntVector2(x, y));
        }
    }

    XMLElement blockElem{ mapElem.GetChild("gridblock") };
    while (blockElem) {

        IntVector3 coords{ blockElem.GetIntVector3("coords") };
        Vector3 position{ (Vector3(coords) - Vector3(mapSize_ * IntVector3(1, 0, 1)) / 2.0f) * blockSize_ + 0.5f * blockSize_ * MapSizeModulo() };
        Quaternion rotation{ blockElem.GetQuaternion("rot") };
        String modelName{ blockSets[blockElem.GetInt("set")][blockElem.GetInt("block")].first_ };
        String materialName{ blockSets[blockElem.GetInt("set")][blockElem.GetInt("block")].second_ };

        if (coords.y_ == 0) {
            obstacles_.Erase(IntVector2(coords.x_, coords.z_));
        }

        if (modelName.Contains("Urho")) {

            spawn->Create<Urho>()->Set(position, rotation);

        } else if (modelName.Contains("Shrimp")) {

            spawn->Create<Shrimp>()->Set(position, rotation);

        } else if (modelName.Contains("Barrel")) {

            spawn->Create<BarrelFactory>()->Set(position, rotation);

        } else if (modelName.Contains("Jelly")) {

            spawn->Create<Jelly>()->Set(position, rotation);

        } else if (modelName.Contains("Crab")) {

            spawn->Create<Crab>()->Set(position, rotation);

        } else {

            Node* blockNode{ node_->CreateChild("Block") };
            blockNode->SetPosition(position);
            blockNode->SetRotation(rotation);

            Block* block{ blockNode->CreateComponent<Block>() };
            block->Initialize(CACHE->GetResource<Model>(modelName),
                              CACHE->GetResource<Material>(materialName));

            if (modelName.Contains("Pit"))
                pits_.Insert(position);

            if (modelName.Contains("PathStraight"))
                if (Random(3))
                    for (int w{Random(8)}; w > 0; --w)
                        spawn->Create<Weed>()->Set(position + blockNode->GetRight() * Random(1.0f, 1.1f) * (Random(2) ?  0.5f : -0.5f) + blockNode->GetUp() * 0.75f + blockNode->GetDirection() * (-0.5f + Random()));

        }

        blockElem = blockElem.GetNext("gridblock");
    }
}

IntVector3 Maze::WorldPositionToCoords(Vector3 position)
{
    return VectorRoundToInt((position - 0.5f * blockSize_ * MapSizeModulo()) / blockSize_ + Vector3(mapSize_ * IntVector3(1, 0, 1)) / 2.0f);
}
bool Maze::IsObstacle(Vector3 position)
{
    return IsObstacle(WorldPositionToCoords(position));
}
bool Maze::IsObstacle(IntVector3 coords)
{
    return obstacles_.Contains(IntVector2(coords.x_, coords.z_));
}

const CameraProperties Maze::GetCameraProperties()
{
    Graphics* graphics{ GetSubsystem<Graphics>() };
    float ratio{ static_cast<float>(graphics->GetWidth()) / graphics->GetHeight() };

    float verticalDistance{ static_cast<float>(mapSize_.z_) };
    float horizontalDistance{ static_cast<float>(mapSize_.x_) / ratio };

    float distance = Max(verticalDistance, horizontalDistance) * 1.05f;

    return { Vector3(0.0f, 23.5f, -4.2f).Normalized() * distance,
             Vector3::DOWN,
             53.0f };
}

Vector3 Maze::MapSizeModulo()
{
    return Vector3(mapSize_.x_ % 2, mapSize_.y_ % 2, mapSize_.z_ % 2);
}
