/* A Mazing Urho
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "urho.h"
#include "hazard.h"


Hazard::Hazard(Context* context) : SceneObject(context)
{
}

void Hazard::OnNodeSet(Node* node)
{
    if (!node)
        return;
}

void Hazard::Update(float timeStep)
{
    if ((node_->GetWorldPosition() - Urho::urho_->GetWorldPosition()).Length() < .666f) {

        PlaySample(CACHE->GetResource<Sound>("Samples/Hit.ogg"), 0.8f);
        Urho::urho_->Hit();
    }
}



