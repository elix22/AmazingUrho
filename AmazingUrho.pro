TARGET = amazingurho

LIBS += \
    ../AmazingUrho/Urho3D/lib/libUrho3D.a \
    -lpthread \
    -ldl \
    -lGL

QMAKE_CXXFLAGS += -std=c++11 -O2

INCLUDEPATH += \
    ../AmazingUrho/Urho3D/include \
    ../AmazingUrho/Urho3D/include/Urho3D/ThirdParty \

TEMPLATE = app
CONFIG -= app_bundle
CONFIG -= qt

HEADERS += \
    luckey.h \
    mastercontrol.h \
    sceneobject.h \
    spawnmaster.h \
    inputmaster.h \
    player.h \
    controllable.h \
    effectmaster.h \
    urho.h \
    maze.h \
    block.h \
    shrimp.h \
    barrelfactory.h \
    barrel.h \
    jelly.h \
    weed.h \
    crab.h \
    hazard.h \
    waterspotlight.h \
    menu.h \
    amazingcam.h

SOURCES += \
    luckey.cpp \
    mastercontrol.cpp \
    sceneobject.cpp \
    spawnmaster.cpp \
    inputmaster.cpp \
    player.cpp \
    controllable.cpp \
    effectmaster.cpp \
    urho.cpp \
    maze.cpp \
    block.cpp \
    shrimp.cpp \
    barrelfactory.cpp \
    barrel.cpp \
    jelly.cpp \
    weed.cpp \
    crab.cpp \
    hazard.cpp \
    waterspotlight.cpp \
    menu.cpp \
    amazingcam.cpp

DISTFILES += \
    LICENSE_TEMPLATE
