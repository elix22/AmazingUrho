/* A Mazing Urho
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include "luckey.h"

namespace Urho3D {
class Node;
class Scene;
}

class AmazingCam;
class Menu;
class Maze;
class Player;

struct CameraProperties{

    Vector3 position_;
    Vector3 lookTarget_;
    float fieldOfView_;
};

enum GameState{ GS_MENU, GS_PLAY, GS_WIN, GS_LOSE };

class MasterControl : public Application
{
    URHO3D_OBJECT(MasterControl, Application);
public:
    MasterControl(Context* context);
    static MasterControl* GetInstance();

    Scene* GetScene() const { return scene_; }
    Maze* GetMaze() const { return maze_; }

    void AddPlayer();
    Player* GetPlayer(int playerId) const;
    Player* GetNearestPlayer(Vector3 pos);
    Vector< SharedPtr<Player> > GetPlayers();
    void RemovePlayer(Player *player);

    // Setup before engine initialization. Modifies the engine paramaters.
    virtual void Setup();
    // Setup after engine initialization.
    virtual void Start();
    // Cleanup after the main loop. Called by Application.
    virtual void Stop();
    void Exit();

    template <class T> void RegisterObject() { context_->RegisterFactory<T>(); }
    template <class T> void RegisterSubsystem() { context_->RegisterSubsystem(new T(context_)); }
    float Sine(const float freq = 1.0f, const float min = -1.0f, const float max = 1.0f, const float shift = 0.0f);
    float Cosine(const float freq = 1.0f, const float min = -1.0f, const float max = 1.0f, const float shift = 0.0f);
    void StartNewGame();
    void Winner();

    void HandleScreenMode(StringHash eventType, VariantMap& eventData);
private:
    void CreateScene();

    static MasterControl* instance_;
    Scene* scene_;
    AmazingCam* camera_;
    Menu* menu_;
    Maze* maze_;
    Vector< SharedPtr<Player> > players_;
    Vector< Node* > spotNodes_;
    Sound* ingameIntro_;
    Sound* ingameLoop_;
    float SinePhase(float freq, float shift);
    void HandleSoundFinished(StringHash eventType, VariantMap& eventData);
};

#endif // MASTERCONTROL_H
