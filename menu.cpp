/* A Mazing Urho
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "menu.h"



Menu::Menu(Context* context) : LogicComponent(context),
    urhoNode_{},
    buttonMaterials_{},
    selectedItem_{0},
    urhoSpeed_{0.0f}
{
}

void Menu::OnNodeSet(Node* node)
{
    if (!node)
        return;


    for (int p{0}; p < 2; ++p) {
        //Poles
        Node* poleNode{ node_->CreateChild("Pole") };
        poleNode->SetPosition(Vector3::RIGHT * (1.7f - 3.4f * p) + Vector3::FORWARD * 0.333f);
        StaticModel* poleModel{ poleNode->CreateComponent<StaticModel>() };
        poleModel->SetModel(CACHE->GetResource<Model>("Models/Pole.mdl"));
        poleModel->SetMaterial(CACHE->GetResource<Material>("Materials/VCol.xml"));
        poleModel->SetCastShadows(true);
    }
    Material* buttonMaterial{ CACHE->GetResource<Material>("Materials/Button.xml") };

    for (int p{0}; p < 3; ++p) {
        //planks
        Node* plankNode{ node_->CreateChild("Button") };
        StaticModel* plankModel{ plankNode->CreateComponent<StaticModel>() };
        String text{};
        switch (p) {
        case 0: text = "Play";
            break;
        case 1: text = "Compete";
            break;
        case 2: text = "Exit";
            break;
        default:
            break;
        }

        plankModel->SetModel(CACHE->GetResource<Model>("Models/Plank_" + text + ".mdl"));
        plankModel->SetMaterial(0, CACHE->GetResource<Material>("Materials/VCol.xml"));

        plankModel->SetMaterial(1, buttonMaterial->Clone());
        plankModel->SetCastShadows(true);

        buttonMaterials_.Push(plankModel->GetMaterial(1));
    }
    Node* terrainNode{ node_->CreateChild("Terrain") };
    StaticModel* terrainModel{ terrainNode->CreateComponent<StaticModel>() };
    terrainModel->SetModel(CACHE->GetResource<Model>("Models/Terrain.mdl"));
    terrainModel->SetMaterial(CACHE->GetResource<Material>("Materials/VCol.xml"));
    terrainModel->SetCastShadows(true);

    Node* titleNode{ node_->CreateChild("Title") };
    StaticModel* titleModel{ titleNode->CreateComponent<StaticModel>() };
    titleModel->SetModel(CACHE->GetResource<Model>("Models/AmazingUrho.mdl"));
    titleModel->SetMaterial(0, CACHE->GetResource<Material>("Materials/VCol.xml"));
    titleModel->SetMaterial(1, CACHE->GetResource<Material>("Materials/Title.xml"));
    titleModel->SetCastShadows(true);

    urhoNode_ = node_->CreateChild("Urho");
    urhoNode_->SetScale(5.0f);
    urhoNode_->SetPosition(Vector3(9.5f, 2.3f, 21.5f));
    urhoNode_->SetRotation(Quaternion(160.0f, Vector3::UP));
    AnimatedModel* urhoModel{ urhoNode_->CreateComponent<AnimatedModel>() };
    urhoModel->SetModel(CACHE->GetResource<Model>("Models/Urho.mdl"));
    urhoModel->ApplyMaterialList();
    urhoModel->SetCastShadows(true);

    urhoNode_->CreateComponent<AnimationController>()->PlayExclusive("Models/Swim.ani", 0, true);
}

void Menu::Update(float timeStep)
{
//    float fade{ Cl};
//    RENDERER->GetViewport(0)->GetRenderPath()->SetShaderParameter("BloomHDRMix", Vector2(0.95f, 0.23f));

    urhoNode_->GetComponent<AnimationController>()->SetSpeed("Models/Swim.ani", 0.17f + urhoSpeed_ * 0.1f);

    for (unsigned b{0}; b < buttonMaterials_.Size(); ++b) {

        Material* mat{ buttonMaterials_[b] };
        mat->SetShaderParameter("MatEmissiveColor", mat->GetShaderParameter("MatEmissiveColor").GetColor().Lerp(
                                    b == selectedItem_ ? Color::WHITE : Color::BLACK, timeStep * 13.0f) );
    }

    if (selectedItem_ == -1) {

        urhoSpeed_ = Lerp(urhoSpeed_, 23.0f, timeStep * (0.23f * urhoSpeed_ + 0.23f));
        urhoNode_->Translate(Vector3::FORWARD * timeStep * urhoSpeed_);
        urhoNode_->Rotate(Quaternion(-timeStep * 2.3f, timeStep * 4.2f * (10.0f - urhoSpeed_ * 0.8f), 0.0f));

        if (!urhoNode_->GetComponent<AnimatedModel>()->IsInView())
            MC->StartNewGame();

        return;
    }

    if (INPUT->GetKeyPress(KEY_UP)
     || INPUT->GetKeyPress(KEY_DOWN))
    {

        selectedItem_ = selectedItem_ == 0 ? 2 : 0;
        SoundSource* source{ node_->GetOrCreateComponent<SoundSource>() };
        source->SetGain(1.0f);
        source->Play(CACHE->GetResource<Sound>("Samples/Select.ogg"));


    } else if (INPUT->GetKeyPress(KEY_RETURN)
            || INPUT->GetKeyPress(KEY_RETURN2)
            || INPUT->GetKeyPress(KEY_KP_ENTER))
    {

        switch (selectedItem_) {
        case 0:
            selectedItem_ = -1;
            break;
        case 1:
            //Start multiplayer game
            break;
        case 2:
            MC->Exit();
            break;
        default:
            break;
        }
    }
}

const CameraProperties Menu::GetCameraProperties()
{
    return { Vector3(-4.2, 3.4f, -17.0f),
             Vector3(4.5, 4.2, -2.3),
                42.0f };
}

void Menu::SetEnabled(bool enable)
{
    node_->SetEnabledRecursive(false);
    Component::SetEnabled(enable);
}



