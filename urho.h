/* A Mazing Urho
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef URHO_H
#define URHO_H

#include "sceneobject.h"

#define SPEED_MIN 1.23f

class Urho : public SceneObject
{
    URHO3D_OBJECT(Urho, SceneObject);
public:
    Urho(Context* context);
    virtual void OnNodeSet(Node* node);
    virtual void Update(float timeStep);

    static Urho* urho_;
    void Set(Vector3 position, Quaternion rotation);
    void Hit();
private:
    Vector3 startPosition_;
    Quaternion startRotation_;
    Vector3 targetPosition_;
    float speed_;

    Node* modelNode_;
};

#endif // URHO_H
