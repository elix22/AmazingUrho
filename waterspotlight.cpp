/* A Mazing Urho
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "waterspotlight.h"


WaterSpotLight::WaterSpotLight(Context* context) : LogicComponent(context),
    first_{false}
{
}

void WaterSpotLight::OnNodeSet(Node* node)
{
    if (!node)
        return;

    node_->SetPosition(Vector3::UP * 17.0f);
    Light* spotLight{ node_->CreateComponent<Light>() };
    spotLight->SetLightType(LIGHT_SPOT);
    spotLight->SetFov(123.0f);
    spotLight->SetRange(42.0f);
    spotLight->SetBrightness(0.34f);
    spotLight->SetShapeTexture(static_cast<Texture*>(CACHE->GetResource<Texture2D>("Textures/Water.png")));
}
void WaterSpotLight::Set(bool first)
{
    first_ = first;
    node_->LookAt(Vector3::DOWN, first_ ? Vector3::FORWARD : Vector3::RIGHT);
    node_->Translate((first_ ? Vector3::RIGHT : Vector3::LEFT) * 5.0f, TS_WORLD);
}

void WaterSpotLight::Update(float timeStep)
{
    node_->Rotate(Quaternion(timeStep * (first_ ? 2.0f : -3.0f), Vector3::UP), TS_WORLD);
    node_->GetComponent<Light>()->SetBrightness(MC->Sine(0.23f, 0.05f, 0.34f, first_ * 0.5f));
}



