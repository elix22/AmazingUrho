/* A Mazing Urho
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "maze.h"

#include "barrel.h"

StaticModelGroup* Barrel::barrelGroup_{};

Barrel::Barrel(Context* context) : Hazard(context)
{
}

void Barrel::OnNodeSet(Node* node)
{
    if (!node)
        return;

    if (!barrelGroup_) {

        barrelGroup_ = node_->GetScene()->CreateComponent<StaticModelGroup>();
        barrelGroup_->SetModel(CACHE->GetResource<Model>("Models/Barrel.mdl"));
        barrelGroup_->SetMaterial(CACHE->GetResource<Material>("Materials/VColHighSpec.xml"));
        barrelGroup_->SetCastShadows(true);
    }

    modelNode_ = node_->CreateChild("Model");
    modelNode_->Rotate(Quaternion(Random(360.0f), Random(2) ? 180.0f : 0.0f, 0.0f));
    barrelGroup_->AddInstanceNode(modelNode_);

}

void Barrel::Update(float timeStep)
{
    Hazard::Update(timeStep);

    float pitDistance = PitDistance();

    if (node_->GetWorldPosition().y_ > 1.5f) {

        node_->Translate(Vector3::DOWN * timeStep * 4.2f);
        modelNode_->Rotate(Quaternion(timeStep * 100.0f, Vector3::RIGHT), TS_PARENT);

    } else {

        node_->Translate(Vector3::FORWARD * timeStep * Min(pitDistance * 5.0f, 1.666f));
        modelNode_->Rotate(Quaternion(timeStep * 235.0f, Vector3::RIGHT), TS_PARENT);
    }

    if (pitDistance < 0.23f)
        node_->Translate(Vector3::DOWN * timeStep * (10.0f - 30.0f * pitDistance));

    if (node_->GetWorldPosition().y_ < -5.0f)
        Disable();
}

float Barrel::PitDistance()
{
    for (Vector3 pitPos: MC->GetMaze()->GetPits()) {

        float distance{ (LucKey::ProjectOntoPlane(pitPos, Vector3::ZERO, Vector3::UP) -
                    LucKey::ProjectOntoPlane(node_->GetWorldPosition(), Vector3::ZERO, Vector3::UP)).Length()};
        if (distance < 1.0f)
            return distance;
    }

    return M_INFINITY;
}



