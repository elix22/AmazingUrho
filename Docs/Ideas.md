#### Ideas

- Anglerfish with bull-like behaviour
- Treasure (bonus points, may be hidden)
- Seaweed patches that function as portals
- Seaweed transition curtain
- Contest mode (two players)
- Gates and keys
- Time and accuracy bonus
- Swim up and down (diagonal in faced direction) after facing your fear of heights