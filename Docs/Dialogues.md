First shrimp eaten on first level (starts timer when closed):
> Urho: "Oh my divine entity!!!| Why didn't anyone tell me shrimp tasted so good?|
> I have to find more of those."

When crossing the path of a crab for the first time:
> Crab: "Snippy snippedy snapin'|, wonder if I could cut yo fin flapin'!"
> Urho: "Yikes!"

At first jellyfish collision:
> Jellyfish: "Whoops, sorry about that!"

First anglerfish encounter (angered/faster when interrupted):
> Anglerfish: "Finally!!| Something to eat!!!| Nice and fat too...|
> But first let me explain how I got so hungry. I owe you that much.|
> You see, I liked how these coins reflected my light when I swam past this opening.
> But when I went through part of it collapsed and I could not get out anymore.

|

> The collapse kept even more light out and made my own light shine like never before  
> and I did not mind it had collapsed.| It was magical...|  
> Only when I got hungry did I start to wonder if I could leave this place to find
> that I could not.| I cried a little.| I'm comfortable telling you that...

|

> ...in part because I kept myself alive with all the seaweed that was in reach.  
> But also because I will eat you now!

When interrupted:
> "You didn't let me finish!!!"

First time anglerfish eats Urho:
> Anglerfish: "Oh my divine entity!!! Why didn't anyone tell me fish tasted so good?|