/* A Mazing Urho
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include "amazingcam.h"



AmazingCam::AmazingCam(Context* context) : LogicComponent(context)
{
}

void AmazingCam::OnNodeSet(Node* node)
{
    if (!node)
        return;

    AUDIO->SetListener(node_->CreateComponent<SoundListener>());
    camera_ = node_->CreateComponent<Camera>();

    Viewport* viewport{ new Viewport(context_, GetScene(), camera_) };
    RenderPath* effectRenderPath{viewport->GetRenderPath()};
    effectRenderPath->Append(CACHE->GetResource<XMLFile>("PostProcess/FXAA3.xml"));
    effectRenderPath->SetEnabled("FXAA3", true);

    effectRenderPath->Append(CACHE->GetResource<XMLFile>("PostProcess/BloomHDR.xml"));
    effectRenderPath->SetShaderParameter("BloomHDRThreshold", 0.34f);
    effectRenderPath->SetShaderParameter("BloomHDRMix", Vector2(0.95f, 0.23f));
    effectRenderPath->SetEnabled("BloomHDR", true);
    viewport->SetRenderPath(effectRenderPath);

    RENDERER->SetViewport(0, viewport);
}

void AmazingCam::Set(CameraProperties camProps)
{
    node_->SetPosition(camProps.position_);
    node_->LookAt(camProps.lookTarget_);
    camera_->SetFov(camProps.fieldOfView_);
}

void AmazingCam::Update(float timeStep)
{
}



