/* A Mazing Urho
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "spawnmaster.h"
#include "barrel.h"

#include "barrelfactory.h"

BarrelFactory::BarrelFactory(Context* context) : SceneObject(context),
    untilBarrel_{Random(1.0f, 3.0f)}
{
}

void BarrelFactory::OnNodeSet(Node* node)
{
    if (!node)
        return;
}

void BarrelFactory::Update(float timeStep)
{
    untilBarrel_ -= timeStep;

    if (untilBarrel_ < 0.0f) {
        SpawnMaster* spawn{ GetSubsystem<SpawnMaster>() };
        spawn->Create<Barrel>()->Set(node_->GetWorldPosition() + Vector3::UP * 5.0f, node_->GetWorldRotation());

        untilBarrel_ = Random(4.0f, 5.0f);
    }
}



