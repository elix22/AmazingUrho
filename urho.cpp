/* A Mazing Urho
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "maze.h"

#include "urho.h"

Urho* Urho::urho_{};

Urho::Urho(Context* context) : SceneObject(context),
    startPosition_{},
    startRotation_{},
    targetPosition_{},
    speed_{SPEED_MIN},
    modelNode_{}
{
}

void Urho::OnNodeSet(Node* node)
{
    if (!node)
        return;

    urho_ = this;

    modelNode_ = node_->CreateChild("UrhoModel");
    modelNode_->SetScale(Vector3(1.23f, 1.0f, 1.0f));
    modelNode_->SetPosition(Vector3::BACK * 0.1f);
    AnimatedModel* urhoModel{ modelNode_->CreateComponent<AnimatedModel>() };
    urhoModel->SetModel(CACHE->GetResource<Model>("Models/Urho.mdl"));
    urhoModel->SetCastShadows(true);
    urhoModel->ApplyMaterialList();
    node_->CreateComponent<AnimationController>()->PlayExclusive("Models/Swim.ani", 0, true);
}

void Urho::Set(Vector3 position, Quaternion rotation)
{
    SceneObject::Set(position, rotation);

    targetPosition_ = startPosition_ = node_->GetWorldPosition();
    startRotation_ = rotation;
}

void Urho::Hit()
{
    speed_ = 0.0f;
    Set(startPosition_, startRotation_);
}
void Urho::Update(float timeStep)
{
    Vector3 toTarget{ targetPosition_ - node_->GetWorldPosition() };
    node_->Translate(toTarget * timeStep * speed_, TS_WORLD);

    if (AnimationController* animCtrl = node_->GetComponent<AnimationController>())
        animCtrl->SetSpeed("Models/Swim.ani", 0.34f * speed_);

    Vector3 newTarget{};
    bool step{ false };

    if (INPUT->GetKeyPress(KEY_UP) || INPUT->GetKeyPress(KEY_W)) {
        newTarget = targetPosition_ + node_->GetDirection();
        step = true;

    } else if (INPUT->GetKeyPress(KEY_LEFT) || INPUT->GetKeyPress(KEY_A)) {
        newTarget = targetPosition_ - node_->GetRight();
        step = true;

    } else if (INPUT->GetKeyPress(KEY_RIGHT) || INPUT->GetKeyPress(KEY_D)) {
        newTarget = targetPosition_ + node_->GetRight();
        step = true;

    }
    if (step) {
        float distance{ toTarget.Length() };
        bool obstacle{ MC->GetMaze()->IsObstacle(newTarget)};

        if (distance < 0.8f && !obstacle) {
            float acceleration{ Pow(1.42f * (1.0f - Abs(distance - 0.23f)), 3.0f) };

            if (acceleration < 0.7f) {
                PlaySample(CACHE->GetResource<Sound>("Samples/Swim0.ogg"), 0.666f);
            } else if (acceleration < 1.4f) {
                PlaySample(CACHE->GetResource<Sound>("Samples/Swim1.ogg"), 0.666f);
            } else if (acceleration < 2.1f) {
                PlaySample(CACHE->GetResource<Sound>("Samples/Swim2.ogg"), 0.666f);
            } else {
                PlaySample(CACHE->GetResource<Sound>("Samples/Swim3.ogg"), 0.666f);
            }

            speed_ += acceleration;

            Vector3 oldDirection{modelNode_->GetWorldDirection()};
            node_->SetDirection(newTarget - targetPosition_);
            modelNode_->SetWorldDirection(oldDirection);
            targetPosition_ = newTarget;

        } else {
            speed_  = Max(SPEED_MIN, speed_ * 0.666f);

            if (obstacle)
                PlaySample(CACHE->GetResource<Sound>("Samples/Bump.ogg"), 0.8f);

        }
    }
    speed_ = Lerp(speed_, SPEED_MIN, timeStep * 1.8f);
    modelNode_->SetRotation(modelNode_->GetRotation().Slerp(Quaternion::IDENTITY, timeStep * speed_ * 2.3f));
}



