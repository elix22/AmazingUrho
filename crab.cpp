/* A Mazing Urho
// Copyright (C) 2018 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "maze.h"

#include "crab.h"

Crab::Crab(Context* context) : Hazard(context),
    pause_{randomizer_}
{
}

void Crab::OnNodeSet(Node* node)
{
    if (!node)
        return;

    animCtrl_ = node_->CreateComponent<AnimationController>();

    modelNode_ = node_->CreateChild("Model");
    AnimatedModel* crabModel{ modelNode_->CreateComponent<AnimatedModel>() };
    crabModel->SetModel(CACHE->GetResource<Model>("Models/Crab.mdl"));
    crabModel->SetCastShadows(true);
    crabModel->SetMaterial(CACHE->GetResource<Material>("Materials/VColHighSpec.xml"));

    animCtrl_->Play("Models/CrabWalk.ani", 0, true);
    animCtrl_->SetTime("Models/CrabWalk.ani", Random(animCtrl_->GetLength("Models/CrabWalk.ani")));
}

void Crab::Update(float timeStep)
{
    Hazard::Update(timeStep);

    if (pause_ > 0.0f) {

        pause_ -= timeStep;

        return;
    }

    animCtrl_->SetSpeed("Models/CrabWalk.ani", node_->GetWorldDirection().DotProduct(modelNode_->GetWorldDirection()));

    node_->Translate(node_->GetDirection() * timeStep, TS_WORLD);
    Vector3 position{ node_->GetWorldPosition() };

    Maze* maze{ MC->GetMaze() };
    if (maze->IsObstacle(position + node_->GetWorldDirection() * 0.5f)) {

        Quaternion turn{ Quaternion(180.0f, Vector3::UP) };
        modelNode_->Rotate(turn.Inverse());
        node_->Rotate(turn);

        animCtrl_->SetSpeed("Models/CrabWalk.ani", 0.0f);

        pause_ = Random(0.5f, 1.0f);
    }

}


